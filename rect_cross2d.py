import numpy as np
from .rect_maxvol import rect_maxvol_qr

def rect_cross2d(A, tol = 1e-2, max_iters = 100, max_restarts = 10, debug_info = False):
    """Computes cross approximation of matrix or matrix-like object in the following form:

.. math::
    A \\approx \\hat{C} \\hat{A} \\hat{R},

    \\hat{C} = C \\hat{A}^\\dagger, \, \\hat{R} = \\hat{A}^\\dagger R,

where :math:`C` contains *basis* columns,

:math:`R` contains *basis* rows,

:math:`\\hat{A}` is an intersection of :math:`C` and :math:`R`

and :math:`\\hat{A}^\\dagger` is a pseudoinverse of :math:`\\hat{A}`.

Implementation updates matrices :math:`\\hat{C}` and :math:`\\hat{R}` iteratively.
Each iteration consist of updating *basis* columns and matrix :math:`\\hat{C}` followed by updating *basis* rows and matrix :math:`\\hat{R}`.
If difference between approximations of two last iterations is small enough, random submatrix is checked for approximation error.
If this check fails, algorithm restarts with already computed *basis* rows and columns.
At the end, computes SVD for cross approximant and returns it.

:Parameters:
    **A**: *ndarray*
        Real or complex matrix.
    **tol**: *float*
        Required accuracy parameter.
    **max_iters**: *integer*
        Maximum number of iterations.
    **max_restarts**: *integer*
        Maximum number of restarts.
    **debug_info**: *bool*
        If true, prints number of iterations and restarts
:Returns:
    **U, S, V**: *ndarrays*
        Singular vectors decomposition of cross approximant

>>> import numpy as np
>>> from scipy.linalg.import hilbert
>>> from rect_cross2d import rect_cross2d
>>> a = hilbert(1000)
>>> U, S, V = rect_cross2d(a, 1e-2, debug_info=1)
cross iters: 2, restarts: 0
>>> print np.linalg.norm(a - U.dot( np.diag(S) ).dot( V ), 2) / np.linalg.norm(a, 2)
0.00227081958873
>>> print U.shape, S.shape, V.shape
(1000, 6) (6,) (6, 1000)
>>> U, S, V = rect_cross2d(a, 1e-5, debug_info=1)
cross iters: 2, restarts: 0
>>> print np.linalg.norm(a - U.dot( np.diag(S) ).dot( V ), 2) / np.linalg.norm(a, 2)
4.26100764586e-06
>>> print U.shape, S.shape, V.shape
(1000, 11) (11,) (11, 1000)
"""
    if min(A.shape) <= 50:
        U, S, V = np.linalg.svd(A, full_matrices = 0)
        rank = 0
        while rank < S.shape[0] and S[rank] > tol*S[0]:
            rank += 1
        return U[:, :rank], S[:rank], V[:rank]
    
    rows = np.random.permutation(np.arange(A.shape[0]))[:10]
    cols, hat_C = rect_maxvol_qr(A[rows].T)
    Q_C, R_C = np.linalg.qr(hat_C)
    rows, hat_R = rect_maxvol_qr(A[:,cols])
    Q_R, R_R = np.linalg.qr(hat_R)
    iters, restarts = 0, 0
    while restarts < max_restarts and iters < max_iters:
        new_cols, new_hat_C = rect_maxvol_qr(A[rows].T)
        new_Q_C, new_R_C = np.linalg.qr(new_hat_C)
        tmp_mat0 = Q_C.T.dot(new_Q_C)
        tmp_mat1 = A[rows].T
        tmp_mat2 = (R_C.dot(tmp_mat1[cols])-tmp_mat0.dot(new_R_C).dot(tmp_mat1[new_cols])).dot(R_R.T)
        tmp_mat3 = (tmp_mat0.T.dot(R_C).dot(tmp_mat1[cols])-new_R_C.dot(tmp_mat1[new_cols])).dot(R_R.T)
        error = np.linalg.norm(tmp_mat2, 2)+np.linalg.norm(tmp_mat3, 2)
        Q_C, R_C, cols = new_Q_C, new_R_C, new_cols
        
        new_rows, new_hat_R = rect_maxvol_qr(A[:,cols])
        new_Q_R, new_R_R = np.linalg.qr(new_hat_R)
        tmp_mat0 = Q_R.T.dot(new_Q_R)
        tmp_mat1 = A[:, cols]
        tmp_mat2 = (R_R.dot(tmp_mat1[rows])-tmp_mat0.dot(new_R_R).dot(tmp_mat1[new_rows])).dot(R_C.T)
        tmp_mat3 = (tmp_mat0.T.dot(R_R).dot(tmp_mat1[rows])-new_R_R.dot(tmp_mat1[new_rows])).dot(R_C.T)
        error += np.linalg.norm(tmp_mat2, 2)+np.linalg.norm(tmp_mat3, 2)
        Q_R, R_R, rows = new_Q_R, new_R_R, new_rows
        iters += 1
        
        if error < tol/2:
            check_rows = np.random.permutation(np.arange(A.shape[0]))[:10]
            if np.linalg.norm(A[check_rows]-Q_R[check_rows].dot(R_R).dot(A[rows][:, cols]).dot(R_C.T).dot(Q_C.T), 2)/np.linalg.norm(A[check_rows], 2) < tol/2:
                break
            rows = np.union1d(check_rows, rows)
            cols, hat_C = rect_maxvol_qr(A[rows].T)
            Q_C, R_C = np.linalg.qr(hat_C)
            rows, hat_R = rect_maxvol_qr(A[:,cols])
            Q_R, R_R = np.linalg.qr(hat_R)
            tmp_mat1 = A[:, cols]
            restarts += 1
            
    U, S, V = np.linalg.svd(R_R.dot(tmp_mat1[rows]).dot(R_C.T))
    rank = 0
    while rank < S.shape[0] and S[rank] > tol/2*S[0]:
        rank += 1
    if debug_info:
        print("cross iters: {}, restarts: {}".format(iters, restarts))
    return Q_R.dot(U[:,:rank]), S[:rank], V[:rank].dot(Q_C.T)
