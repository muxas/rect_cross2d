""".. moduleauthor:: Alexander Mikhalev <muxasizhevsk@gmail.com>

:mod:`rect_cross2d` module contains routines to build approximations of matrices or matrix-like objects (i.e. matrix of function values).
All routines are based on `rect_maxvol` ( https://bitbucket.org/muxas/rect_maxvol ) module, that finds good rows and columns to build cross approximation.

Functions
=========
"""

__all__ = ['rect_cross2d']
from .rect_cross2d import *
