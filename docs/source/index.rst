.. rect_cross2d documentation master file, created by
   sphinx-quickstart on Mon Aug 18 14:49:21 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Synopsis of the :mod:`rect_cross2d` module
==========================================

.. automodule:: rect_cross2d
   :members:
