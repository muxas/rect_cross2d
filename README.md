# What is this repository for?

Module *rect_cross2d* is a useful tool to build low-rank approximations of different matrices.
These approximations are based on rectangular pseudoskeleton approximation theory, which is extension of regular skeleton approximations.
With help of *rect_maxvol* submodule, function `rect_cross2d` computes *basis* rows and *basis* columns, builds corresponding pseudoskeleton approximation and compresses it with SVD.

# Documentation

Documentation and examples are available at http://rect-cross2d.readthedocs.org/
Also, there is standalone ipython notebook example *examples/example.ipynb*

# Requirements

All the package is written in pure python and requires only *numpy* and *scipy*.
Examples can run without any compilation of the source.
However, package `rect_maxvol`, included in repository as a submodule, can be accelerated dramatically by compiling its source with *cython*.

# Installation

If you want to install package into system, just run:
`python setup.py install`

# Python version support

Current implementation was succesfully tested with numpy 1.9.0,
scipy 0.14.0, cython 0.22 and ipython-notebook 3.1.0 for Python 2.7.9
and Python 3.4.3

# Main Contributor

Alexander Mikhalev <muxasizhevsk@gmail.com>
